const courses_const = [
    {
        key: 1,
        img_src : "img/course01.jpg",
        link_href : "#",
        title : "Beginner to Pro in Excel: Financial Modeling and Valuation",
        category : "Business",
        price :"Free"
    },
    {
        key: 2,
        img_src : "img/course02.jpg",
        link_href : "#",
        title : "Introduction to CSS ",
        category : "Web Design",
        price :"Premium"
    },
    {
        key: 3,
        img_src : "img/course03.jpg",
        link_href : "#",
        title : "The Ultimate Drawing Course | From Beginner To Advanced",
        category : "Drawing",
        price :"Premium"
    },
    {
        key: 4,
        img_src : "img/course04.jpg",
        link_href : "#",
        title : "The Complete Web Development Course",
        category : "Web Development",
        price :"Free"
    },
    {
        key: 5,
        img_src : "img/course05.jpg",
        link_href : "#",
        title : "PHP Tips, Tricks, and Techniques",
        category : "Web Development",
        price :"Free"
    },
    {
        key: 6,
        img_src : "img/course06.jpg",
        link_href : "#",
        title : "All You Need To Know About Web Design",
        category : "Web Design",
        price :"Free"
    },
    {
        key: 7,
        img_src : "img/course07.jpg",
        link_href : "#",
        title : "How to Get Started in Photography",
        category : "Photography",
        price :"Free"
    },
    {
        key: 8,
        img_src : "img/course08.jpg",
        link_href : "#",
        title : "Typography From A to Z",
        category : "Typography",
        price :"Free"
    }
]

export default courses_const;