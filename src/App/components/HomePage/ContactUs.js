import React from 'react';

const ContactUs = () => (
    <div id="contact-cta" className="section">
        <div className="bg-image bg-parallax overlay" style={{ backgroundImage: 'url(img/cta2-background.jpg)' }}></div>
        <div className="container">
            <div className="row">
                <div className="col-md-8 col-md-offset-2 text-center">
                    <h2 className="white-text">Contact Us</h2>
                    <p className="lead white-text">Libris vivendo eloquentiam ex ius, nec id splendide abhorreant.</p>
                    <a className="main-button icon-button" href="#">Contact Us Now</a>
                </div>
            </div>
        </div>
    </div>
);

export default ContactUs;