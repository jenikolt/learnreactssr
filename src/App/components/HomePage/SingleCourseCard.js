import React, { Component } from 'react';

export default class SingleCourseCard extends Component {
constructor(props){
    super(props)
    this.state = {
        course_key: this.props.course_info_key,
        course_img_src : this.props.course_info.img_src,
        course_link_href : this.props.course_info.link_href,
        course_title : this.props.course_info.title,
        course_category : this.props.course_info.category,
        course_price : this.props.course_info.price
    }
}

    render(){
        return(
            <div className="col-md-3 col-sm-6 col-xs-6">
							<div className="course">
								<a href={this.state.course_link_href} className="course-img">
									<img src={this.state.course_img_src} alt="" />
									<i className="course-link-icon fa fa-link"></i>
								</a>
								<a className="course-title" href={this.state.course_link_href}>{this.state.course_title}</a>
								<div className="course-details">
									<span className="course-category">{this.state.course_category}</span>
									{this.state.course_price === "Free" ? <span className='course-price course-free'>Free</span> : <span className='course-price course-premium'>Premium</span>} 
								</div>
							</div>
			</div>
        );
    }
}