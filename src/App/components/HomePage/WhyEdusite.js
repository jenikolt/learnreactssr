import React from 'react';


const Feature = (title, text) => (
    <div className="col-md-4">
        <div className="feature">
            <i className="feature-icon fa fa-flask"></i>
            <div className="feature-content">
                <h4>{title}</h4>
                <p>{text}</p>
            </div>
        </div>
    </div>
);

const features = [
    {
        title: 'Online Courses',
        text: 'Ceteros fuisset mei no, soleat epicurei adipiscing ne vis. Et his suas veniam nominati.'
    },
    {
        title: 'Expert Teachers',
        text: 'Ceteros fuisset mei no, soleat epicurei adipiscing ne vis. Et his suas veniam nominati.'
    },
    {
        title: 'Community',
        text: 'Ceteros fuisset mei no, soleat epicurei adipiscing ne vis. Et his suas veniam nominati.'
    }
]

const WhyEdusite = () => (
    <div id="why-us" className="section">
        <div className="container">

            <div className="row">
                <div className="section-header text-center">
                    <h2>Why Edusite</h2>
                    <p className="lead">Libris vivendo eloquentiam ex ius, nec id splendide abhorreant.</p>
                </div>
                {Feature(features[0].title, features[0].text)}
                {Feature(features[1].title, features[1].text)}
                {Feature(features[2].title, features[2].text)}
            </div>
        </div>
    </div>
);

export default WhyEdusite;