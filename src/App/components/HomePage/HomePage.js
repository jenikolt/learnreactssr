import React, { Component } from 'react';
import CourseList from './CourseList';
import CallToAction from './CallToAction';
import WhyEdusite from './WhyEdusite';
import ContactUs from './ContactUs';



class HomePage extends Component {
    render() {
        const instyle = { backgroundImage: "url('./img/home-background.jpg')" };
        return (
            <div>
            <div id="home" className="hero-area">
                <div className="bg-image bg-parallax overlay" style={instyle}></div>
                <div className="home-wrapper">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-8">
                                <h1 className="white-text">Edusite Free Online Training Courses</h1>
                                <p className="lead white-text">Libris vivendo eloquentiam ex ius, nec id splendide abhorreant, eu pro alii error homero.</p>
                                <a className="main-button icon-button" href="/">Get Started!</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <CourseList />
            <CallToAction />
            <WhyEdusite />
            <ContactUs />
            </div>
        );
    }
}

export default HomePage;
