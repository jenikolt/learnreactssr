import React, { Component } from 'react';
import SingleCourseCard from './SingleCourseCard';
import courses_const  from './courses_const';

export default class CourseList extends Component {
    render(){
        //вывод по простому, чтобы не задерживаться
        const index_first = 0;
        const countcard = 4; 
        const row_1 = courses_const.slice(index_first, index_first+countcard);
        const row_2 = courses_const.slice(index_first+countcard);
        return(
            <div id="courses" className="section">
            <div className="container">
            <div className="row">
					<div className="section-header text-center">
						<h2>Explore Courses</h2>
						<p className="lead">Libris vivendo eloquentiam ex ius, nec id splendide abhorreant.</p>
					</div>
				</div>
                <div id="courses-wrapper">
                    <div className="row">{row_1.map(item => <SingleCourseCard key={item.key} course_info = {item}/>)}</div>
                    <div className="row">{row_2.map(item => <SingleCourseCard key={item.key} course_info = {item}/>)}</div>
                </div>
                <div className="row">
					<div className="center-btn">
						<a className="main-button icon-button" href="#">More Courses</a>
					</div>
				</div>
            </div>
            </div>
        );
    }
}

