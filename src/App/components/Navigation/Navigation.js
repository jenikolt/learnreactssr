import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

export default class Navigation extends Component{
    render(){
		const classtext = this.props.isfooter ? "footer-nav" : "main-menu nav navbar-nav navbar-right";
        return(
					<ul className={classtext}>
						<li><NavLink to='/'>Home</NavLink></li>
						<li><a href="/">About</a></li>
						<li><a href="/">Courses</a></li>
						<li><NavLink to='/Blog'>Blog</NavLink></li>
						<li><NavLink to='/Contact'>Contact</NavLink></li>
					</ul>
        );
    }
}

