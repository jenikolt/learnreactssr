import React, { Component } from 'react';
import Navigation from '../Navigation/Navigation';

export default class Header extends Component{
    render(){
        return(
            <header id="header" className="transparent-nav">
                <div className='container'>
                <div className="navbar-header">
					
					<div className="navbar-brand">
						<a className="logo" href="index.html">
							<img src="img/logo-alt.png" alt="logo" />
						</a>
					</div>
					

				
					<button className="navbar-toggle">
						<span></span>
					</button>
				</div>
                <nav id="nav">
                <Navigation />
                </nav>
                </div>
            </header>
        );
    }
}

