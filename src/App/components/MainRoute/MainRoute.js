import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import HomePage from '../HomePage/HomePage';
import BlogsListPage from '../BlogsListPage/BlogsListPage';
import BlogViewPage from '../BlogViewPage/BlogViewPage';
import ContactPage from '../ContactPage/ContactPage';


class MainRoute extends Component {
render(){
    return(
    <Switch>
        <Route exact path='/' component={HomePage}></Route>
        <Route path='/Blog' component={BlogsListPage}></Route>
        <Route path='/Blog-Post' component={BlogViewPage}></Route>
        <Route path='/Contact' component={ContactPage}></Route>
    </Switch>
    );
}
}


export default MainRoute;
