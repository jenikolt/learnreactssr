import React, { Component } from 'react';
import Header from './components/Header/Header';
import Footer from './components/Footer/Footer';
import MainRoute from './components/MainRoute/MainRoute';
import { Provider } from 'react-redux';
import { createStore } from 'redux';

const blogsList = ['blogtext1', 'blogtext2', 'blogtext3'];
const reducer = (state = blogsList, action) => {
  return state;
}
const store = createStore(reducer);
window.store = store;


class App extends Component {
  render() {
    return (

      <Provider store={store}>
        <div>
          <Header />
          <MainRoute />
          <Footer />
          <div id='preloader' style={{display: "none"}}>
			      <div className='preloader'></div>
		      </div>
        </div>
      </Provider>

    );
  }
}

export default App;
